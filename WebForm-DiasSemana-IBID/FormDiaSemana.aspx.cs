﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebForm_DiasSemana_IBID
{
    public partial class FormDiaSemana : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnSelecionar_Click(object sender, EventArgs e)
        {
            string diasSelecionados = "Dias de trabalho: ";

            foreach (ListItem item in ckbDiaSemana.Items)
            {
                if (item.Selected)
                {
                    diasSelecionados += item.Text + ", ";
                }
            }

            diasSelecionados = diasSelecionados.TrimEnd(',');
            lblDiasSelecionados.Text = diasSelecionados;
        }
    }
}