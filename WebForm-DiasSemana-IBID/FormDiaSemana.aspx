﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FormDiaSemana.aspx.cs" Inherits="WebForm_DiasSemana_IBID.FormDiaSemana" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Dias da Semana</title>
    <link rel="stylesheet" type="text/css" href="Assets/css/form-style.css" />
    <style type="text/css">
        .auto-style1 {
            height: 43px;
        }
        .auto-style2 {
            height: 27px;
        }
        .auto-style3 {
            height: 30px;
            background-color: #CAC4CE;
            border-radius: 10px;
            padding: 10px;
            margin-top: 20px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="main-div">
            <table style="width: 100%;">
                <h1 class="auto-style2">Dias da semana</h1>
                <tr>
                    <td class="auto-style1">Escolha os dias da semana que você irá trabalhar:</td>
                </tr>
                <tr>
                    <td>
                    <asp:CheckBoxList ID="ckbDiaSemana" runat="server">
                        <asp:ListItem Value="1">Segunda-feira</asp:ListItem>
                        <asp:ListItem Value="2">Terça-feira</asp:ListItem>
                        <asp:ListItem Value="3">Quarta-feira</asp:ListItem>
                        <asp:ListItem Value="4">Quinta-feira</asp:ListItem>
                        <asp:ListItem Value="5">Sexta-feira</asp:ListItem>
                        <asp:ListItem Value="6">Sábado</asp:ListItem>
                    </asp:CheckBoxList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSelecionar" runat="server" Text="Selecionar" class="botao-selecionar" OnClick="btnSelecionar_Click" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style3">
                        <asp:Label ID="lblDiasSelecionados" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
            
        </div>
    </form>
</body>
</html>
